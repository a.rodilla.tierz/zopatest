import sys
import csv

from entities.lender import Lender

class SearchLender:

    def __init__(self):
        self.lendersListWork = []

    def importCSV(self, pathFile):
        try:
            with open(pathFile, 'rt') as csvfile:
                spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
                for row in spamreader:
                    if len(row) > 0 and len(row[0]) > 0:
                        self.procesateLender(row)
            csvfile.close()
        except IOError:
            pass
        except:
            print("Unexpected error:", sys.exc_info())
            # print('not valit row: ', row)
        return self.lendersListWork

    def procesateLender(self, datas):
        if (u""+datas[2]).isnumeric() :
            self.lendersListWork.append(Lender(datas[0], datas[1], datas[2]))
