import sys
import math
from entities.lender import Lender

class ChooserLender:

    def __init__(self, listLenders, amount):
        self.listLenders = listLenders
        self.amount = amount

    def getListLenders(self):
        self.listLenders.sort(key=lambda x: x.rate)

    def pieceNode(self):
        listUsableLender = []
        workAmount = self.amount
        for nodeLender in self.listLenders:
            if nodeLender.avaliable > workAmount:
                listUsableLender.append(Lender(nodeLender.name, nodeLender.rate, workAmount))
                workAmount = 0
            else:
                workAmount = (workAmount - nodeLender.avaliable) if workAmount - nodeLender.avaliable else 0
                listUsableLender.append(nodeLender)
            if workAmount == 0:
                break

        return listUsableLender if workAmount == 0 else []
