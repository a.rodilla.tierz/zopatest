import sys

from bussines.csvReader import SearchLender
from bussines.chooserLenders import ChooserLender
from bussines.builderLenderCombination import CombinationLenders

class LogicRoute:
    
    def __init__(self, pathFile, amount):
        if (u""+amount).isnumeric():
            amount = int(amount)
            if (amount < 1000 or amount > 15000):
                sys.exit(('not valit amount:', amount))
        else:
            sys.exit(('not valit format amount:', amount))

        reader = SearchLender()
        lendersReference = reader.importCSV(pathFile)
        if len(lendersReference) == 0 :
            sys.exit(('can\'t open or can\'t found file:', pathFile))
        else:
            usableLenders = ChooserLender(lendersReference, amount)
            usableLenders.getListLenders()
            listChoosedLenders = usableLenders.pieceNode()

            if len(listChoosedLenders) == 0 :
                sys.exit(('Sorry, market have not enough providers or you request:', amount))

            workCombiner = CombinationLenders()
            workCombiner.combineFranctionOfLenders(listChoosedLenders, amount)
            workCombiner.drawFinalResponse()
