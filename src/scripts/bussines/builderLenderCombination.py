import sys
import math
from entities.lender import Lender
from bussines.ratesManager import RateManager

class CombinationLenders:

    def __init__(self):
        self.rateWorker = RateManager()
        self.finalRate = 0
        self.finalMonthly = 0
        self.finalamount = 0
        self.initialamount = 0

    def combineFranctionOfLenders(self, listChoosedLenders, amount):
        self.initialamount = amount
        for item in listChoosedLenders:
            rateAmount = self.rateWorker.determinateRateByAmount((item.rate / 12), item.avaliable, 36)
            fractionRate = (float(item.avaliable) / amount)
            self.finalMonthly += rateAmount
            self.finalamount += (rateAmount * 36)
            self.finalRate += (fractionRate * item.rate)
        return (self.initialamount, self.finalMonthly, self.finalamount, self.finalRate)

    def drawFinalResponse(self):
        print 'Requested amount:', unichr(163) + str(round(self.initialamount, 2))
        print 'Annual Interest Rate:', str(round((self.finalRate * 100), 1)) + '%'
        print 'Monthly repayment:', unichr(163) + str(round(self.finalMonthly, 2))
        print 'Total repayment:', unichr(163) + str(round(self.finalamount, 2))
