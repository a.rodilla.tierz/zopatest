import sys
import math


class RateManager:

    def determinateRateByAmount(self, rateWork, amount, months):
        checkVal1 = amount * rateWork
        return checkVal1 + (checkVal1 / self.determinateDivisor(rateWork, months))

    def determinateDivisor(self, rateWork, months):
        return ((1 + rateWork) ** months) - 1
