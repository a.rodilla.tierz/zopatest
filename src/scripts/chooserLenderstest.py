import unittest
from bussines.chooserLenders import ChooserLender
from bussines.csvReader import SearchLender

class TestChooeserLenders(unittest.TestCase):

    def test_get_lenders(self):
        pathFile = 'src/media/zoopaLenders.csv'
        reader = SearchLender()
        amount = 500
        lendersReference = reader.importCSV(pathFile)
        usableLenders = ChooserLender(lendersReference, amount)
        usableLenders.getListLenders()
        listChoosedLenders = usableLenders.pieceNode()
        self.assertEqual(len(listChoosedLenders), 2)
        self.assertEqual(listChoosedLenders[1].avaliable, 20)

    def test_get_lenders2(self):
        pathFile = 'src/media/zoopaLenders.csv'
        reader = SearchLender()
        amount = 1050
        lendersReference = reader.importCSV(pathFile)
        usableLenders = ChooserLender(lendersReference, amount)
        usableLenders.getListLenders()
        listChoosedLenders = usableLenders.pieceNode()
        self.assertEqual(len(listChoosedLenders), 3)
        self.assertEqual(listChoosedLenders[2].avaliable, 50)

    def test_get_lenders3(self):
        pathFile = 'src/media/zoopaLenders.csv'
        reader = SearchLender()
        amount = 5050
        lendersReference = reader.importCSV(pathFile)
        usableLenders = ChooserLender(lendersReference, amount)
        usableLenders.getListLenders()
        listChoosedLenders = usableLenders.pieceNode()
        self.assertEqual(len(listChoosedLenders), 0)

if __name__ == '__main__':
    unittest.main()
