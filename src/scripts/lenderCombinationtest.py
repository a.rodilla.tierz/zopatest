import unittest
from bussines.chooserLenders import ChooserLender
from bussines.csvReader import SearchLender
from bussines.builderLenderCombination import CombinationLenders
from entities.lender import Lender

class TestLendersCombination(unittest.TestCase):

    def test_combine_lenders(self):
        amount = 450
        listMockLends = [
                        Lender('AA', 0.08, 150),
                        Lender('BB', 0.09, 50),
                        Lender('CC', 0.10, 250)
                        ]

        workCombiner = CombinationLenders()
        lendInformation = workCombiner.combineFranctionOfLenders(listMockLends, amount)
        self.assertEqual(lendInformation[0], 450)
        self.assertEqual(round(lendInformation[1], 2), 14.36)
        self.assertEqual(round(lendInformation[2], 2), 516.86)
        self.assertEqual(round(lendInformation[3], 3), 0.092)

if __name__ == '__main__':
    unittest.main()
