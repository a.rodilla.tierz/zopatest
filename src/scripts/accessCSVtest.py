import unittest
# from scripts.zopaApp import pathFile
from bussines.csvReader import SearchLender
# from csvReader import SearchLender

class TestAccessCSV(unittest.TestCase):

    def test_get_csv_lenders(self):
        pathFile = 'src/media/zoopaLenders.csv'
        reader = SearchLender()
        lendersReference = reader.importCSV(pathFile)
        self.assertEqual(len(lendersReference), 7)

    def test_get_csv_lenders_fail(self):
        pathFile = 'src/media/zoopaLendersEmpty.csv'
        reader = SearchLender()
        lendersReference = reader.importCSV(pathFile)
        self.assertEqual(len(lendersReference), 0)

    def test_get_csv_lenders_fail_2(self):
        pathFile = 'opaLendersEmpty.csv'
        reader = SearchLender()
        lendersReference = reader.importCSV(pathFile)
        self.assertEqual(len(lendersReference), 0)

    def test_get_csv_lenders_fail_3(self):
        pathFile = 'src/media/zoopaLendersWorng.csv'
        reader = SearchLender()
        lendersReference = reader.importCSV(pathFile)
        self.assertEqual(len(lendersReference), 0)


if __name__ == '__main__':
    unittest.main()
