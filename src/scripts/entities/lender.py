import sys
class Lender:

    def __init__(self, name, rate, avaliable):
        self.name = name
        self.rate = float(rate) if float(rate) != None else 0.0
        self.avaliable = int(avaliable) if int(avaliable) != None else 0
